const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/', createUserValid, function(req, res, next) {
    const user = UserService.createUser(req.body);

    if(user) {
        res.data = user;
    } else {
        res.err = {
            error: true,
            message: "Sorry, we can\'t create user."
        }
    }

    next();
}, responseMiddleware);

router.get('/', function(req, res, next) {
    const allUsers = UserService.getAllUsers();

    if(allUsers) {
        res.data = allUsers;
    } else {
        res.err = {
            error: true,
            message: "Sorry, we can\'t get all users from db."
        }
    }

    next();
}, responseMiddleware);

router.get('/:id', function(req, res, next) {
    const user = UserService.search({ id: req.params.id });

    if(user) {
        res.data = user;
    } else {
        res.err = {
            error: true,
            message: "Sorry, we can\'t get current user, no such ID in db."
        }
    }
    
    next();
}, responseMiddleware);

router.put('/:id', updateUserValid, function(req, res, next) {
    const user = UserService.updateUser(req.params.id, req.body);

    if(user) {
        res.data = user;
    } else {
        res.err = {
            error: true,
            message: "Sorry, we can\'t update current user."
        }
    }

    next();
}, responseMiddleware);

router.delete('/:id', function(req, res, next) {
    const data = UserService.deleteUser(req.params.id);
    if(data) {
        res.data = data;
    } else {
        res.err = {
            error: true,
            message: "Sorry, we can\'t delete current user."
        }
    }

    next();
}, responseMiddleware);

module.exports = router;