const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.post('/', createFighterValid, function(req, res, next) {
    const fighter = FighterService.createFighter(req.body);

    if(fighter) {
        res.data = fighter;
    } else {
        res.err = {
            error: true,
            message: "Sorry, we can\'t create fighter."
        }
    }

    next();
}, responseMiddleware);

router.get('/', function(req, res, next) {
    const allFighters = FighterService.getAllFighters();

    if(allFighters) {
        res.data = allFighters;
    } else {
        res.err = {
            error: true,
            message: "Sorry, we can\'t get all fighters."
        }
    }

    next();
}, responseMiddleware);

router.get('/:id', function(req, res, next) {
    const fighter = FighterService.searchFighter({ id: req.params.id });

    if(fighter) {
        res.data = fighter;
    } else {
        res.err = {
            error: true,
            message: "Sorry, we can\'t get current fighter, no such ID in db."
        }
    }

    next();
}, responseMiddleware);

router.put('/:id', updateFighterValid, function(req, res, next) {
    const fighter = FighterService.updateFighter(req.params.id, req.body);

    if(fighter) {
        res.data = fighter;
    } else {
        res.err = {
            error: true,
            message: "Sorry, we can\'t update current fighter."
        }
    }

    next();
}, responseMiddleware);

router.delete('/:id', function(req, res, next) {
    const fighter = FighterService.deleteFighter(req.params.id);

    if(fighter) {
        res.data = fighter;
    } else {
        res.err = {
            error: true,
            message: "Sorry, we can\'t delete current fighter."
        }
    }
    
    next();
}, responseMiddleware);

module.exports = router;