const { user } = require('../models/user');
const UserService = require('../services/userService');

const createUserValid = (req, res, next) => {
    const errorMessage = newUserValidation(req.body);
    
    if(errorMessage) {
        res.status(400).send({
            error: true,
            message: errorMessage
        });
    } else {
        next();
    }
}

const updateUserValid = (req, res, next) => {
    const errorMessage = updateUserValidation(req.body, req.params.id);
    
    if(errorMessage) {
        res.status(400).send({
            error: true,
            message: errorMessage
        });
    } else {
        next();
    }
}

function newUserValidation(newUser) {
    if(Object.keys(newUser).length === 0 && newUser.constructor === Object) {
        return 'All fields should be filled.'
    }

    for(let key in newUser) {
        if(!(key in user) || ('id' in newUser)) {
            return 'There should not be extra fields in the form.';
        }
    }

    for(let key in user) {
        if(key !== 'id') {
            if(!newUser[key] || !newUser[key].length) {
                return 'One of the fields is empty. Please fill in all fields.';
            }
        }
    }

    if(('email' in newUser) && !newUser.email.endsWith('@gmail.com')) {
        return 'We support only GMAIL service emails.';
    }

    if(('email' in newUser) && UserService.search({email: newUser.email})){
        return 'User with such email already exist.';
    }

    if(('phoneNumber' in newUser) && !(newUser.phoneNumber.startsWith('+380') && newUser.phoneNumber.length === 13)) {
       return 'Phone number should be in +380xxxxxxxxx format.';
    }

    if(('password' in newUser) && newUser.password.length <= 2) {
        return 'Password should contains at least 3 symbols.';
    }
}

function updateUserValidation(updateUser, usedId) {
    if(!UserService.search({id: usedId})) {
        return 'Update user error. We cannot update a nonexistent user.';
    }

    for(let key in updateUser) {
        if(!(key in user) || ('id' in updateUser)) {
            return 'Update user error. There should not be extra fields in the form.';
        }
    }

    for(let key in updateUser) {
        if(!updateUser[key].length) {
            return 'Update user error. One of the fields is empty. Please fill in all fields.';
        }
    }

    if(('email' in updateUser) && !updateUser.email.endsWith('@gmail.com')) {
        return 'Update user error. We support only GMAIL service emails.';
    }

    if(('email' in updateUser) && UserService.search({email: updateUser.email})){
        return 'Update user error. User with such email already exist.';
    }

    if(('phoneNumber' in updateUser) && !(updateUser.phoneNumber.startsWith('+380') && updateUser.phoneNumber.length === 13)) {
       return 'Update user error. Phone number should be in +380xxxxxxxxx format.';
    }

    if(('password' in updateUser) && updateUser.password.length <= 2) {
        return 'Update user error. Password should contains at least 3 symbols.';
    }
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;