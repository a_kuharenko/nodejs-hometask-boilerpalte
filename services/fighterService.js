const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    createFighter(fighter) {
        const item = FighterRepository.create(fighter);
        
        return item;
    }

    getAllFighters() {
        const items = FighterRepository.getAll();
        if(!items) {
            return null;
        }
        return items;
    }

    updateFighter(id, fighter) {
        const item = FighterRepository.update(id, fighter);
        if(!item) {
            return null;
        }
        return item;
    }

    deleteFighter(id) {
        const item = FighterRepository.delete(id);
        if(!item) {
            return null;
        }
        return item;
    }

    searchFighter(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new FighterService();