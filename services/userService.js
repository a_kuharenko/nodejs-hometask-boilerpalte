const { UserRepository } = require('../repositories/userRepository');

class UserService {

    createUser(user) {
        const item = UserRepository.create(user);
        
        return item;
    }

    getAllUsers() {
        const items = UserRepository.getAll();
        if(!items) {
            return null;
        }
        return items;
    }

    updateUser(id, user) {
        const item = UserRepository.update(id, user);
        if(!item) {
            return null;
        }
        return item;
    }

    deleteUser(id) {
        const item = UserRepository.delete(id);
        if(!item) {
            return null;
        }
        return item;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new UserService();